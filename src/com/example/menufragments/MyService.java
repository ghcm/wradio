package com.example.menufragments;

import java.util.concurrent.TimeUnit;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class MyService extends Service {

    MyPlayer player = new MyPlayer();

    final String LOG_TAG = "myLogs";

    public void onCreate() {
        super.onCreate();
        Log.d(LOG_TAG, "onCreate");
        // Toast.makeText(this, "onCreate", Toast.LENGTH_SHORT).show();
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        //Toast.makeText(this, "onStartCommand", Toast.LENGTH_SHORT).show();
        Log.d(LOG_TAG, "onStartCommand");
        someTask();
        return super.onStartCommand(intent, flags, startId);
    }

    public void onDestroy() {
        super.onDestroy();
        player.onDestroy();
        Log.d(LOG_TAG, "onDestroy");
        //Toast.makeText(this, "onDestroy", Toast.LENGTH_SHORT).show();
    }

    public IBinder onBind(Intent intent) {
        Log.d(LOG_TAG, "onBind");
        //Toast.makeText(this, "onBind", Toast.LENGTH_SHORT).show();
        return null;
    }

    void someTask() {
        new Thread(new Runnable() {
            public void run() {

                player.play();

            }
        }).start();
    }

}
