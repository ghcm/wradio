package com.example.menufragments;

import java.util.HashMap;


import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class MenuItemFragment extends Fragment {
	
	
	protected ImageLoader imageLoader = ImageLoader.getInstance();
	TextView titleText;
	TextView priceText;
	TextView desText;
	ImageView imageElement;

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
	  
	  View v = inflater.inflate(R.layout.menu_item, container, false);
      
    return v;
  }
  
  @Override
  public void onActivityCreated (Bundle savedInstanceState) {
      super.onActivityCreated(savedInstanceState);
      
      HashMap item = MainActivity.getMenuItem();
  	ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getActivity()).build();
	 
    ImageLoader.getInstance().init(config);
      
      
      String title = (String) item.get("title");
		String price = (String) item.get("price");
		String des = (String) item.get("des");
		String image = (String) item.get("image");
		
		TextView titleText = (TextView) getView().findViewById(R.id.title);
		TextView priceText = (TextView) getView().findViewById(R.id.price);
		TextView desText = (TextView) getView().findViewById(R.id.des);
		ImageView imageElement = (ImageView) getView().findViewById(R.id.image);

		titleText.setText(title);
		priceText.setText("Цена: " + price);
		desText.setText("Описание: " + des);
		
		
		imageLoader.displayImage(image, imageElement);
  }
  

}