package com.example.menufragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Fragment;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SimpleExpandableListAdapter;
import android.widget.TextView;
import android.widget.ExpandableListView.OnChildClickListener;

public class MenuMainFragment extends Fragment {
	
	final String LOG_TAG = "myLogs";
	  
	  ExpandableListView elvMain;
	  MenuAdapterHelper ah;
	  SimpleExpandableListAdapter adapter;
	  TextView tvInfo;
	  
	  Context ctx;
	  
	  JSONObject jsonitem;
	  JSONObject jsonobject;
	  JSONArray jsonarray;
	  
	  View v;
	  
	  JSONArray jsonitems;
	  
	  public LinearLayout progressBar;
	  
	  // коллекция для элементов одной группы
	  ArrayList<Map<String, String>> childDataItem;

	  // общая коллекция для коллекций элементов
	  ArrayList<ArrayList<Map<String, String>>> childData;
	  
	  Map<String, String> m;
	  
	  ArrayList<Map<String, String>> arraylist;

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
	  
	  v = inflater.inflate(R.layout.menu_main_layout, null);
	  
	  progressBar = (LinearLayout) v.findViewById(R.id.llLayout);
	  progressBar.setVisibility(View.VISIBLE);
	  
	  ctx = getActivity();
	  
	  new DownloadJSON().execute();
	  
    return v;
  }
  
  
  
  public Object getItem(int position) {
      return arraylist.get(position);
   }
  
  public Object getChild(int position) {
      return childData.get(position);
   }
  
  public HashMap getChildItem(int position) {
      return (HashMap) childDataItem.get(position);
   }
  
  
  
  
  private class DownloadJSON extends AsyncTask<Void, Void, Void> {
  	 
      @Override
      protected void onPreExecute() {
          super.onPreExecute();        
      }

      @Override
      protected Void doInBackground(Void... params) {
          // Create an array
          arraylist = new ArrayList<Map<String, String>>();
          // Retrieve JSON Objects from the given URL address
          jsonobject = JSONfunctions
                  .getJSONfromURL("http://easyscript.me/api/menu/");
                   
          try {
              // Locate the array name in JSON
              jsonarray = jsonobject.getJSONArray("menu");
              
              childData = new ArrayList<ArrayList<Map<String, String>>>();
              
              for (int i = 0; i < jsonarray.length(); i++) {
                  HashMap<String, String> map = new HashMap<String, String>();
                  jsonobject = jsonarray.getJSONObject(i);
             
                 map.put("groupName", jsonobject.getString("title"));                   
                 jsonitems = jsonobject.getJSONArray("items"); 
               
                 childDataItem = new ArrayList<Map<String, String>>();
                 for (int y = 0; y < jsonitems.length(); y++) { 
              	   jsonitem = jsonitems.getJSONObject(y);
                       m = new HashMap<String, String>();
                       m.put("title", jsonitem.getString("title")); 
                       m.put("price", jsonitem.getString("price")); 
                       m.put("des", jsonitem.getString("des")); 
                       m.put("image", jsonitem.getString("image")); 
                    
                       childDataItem.add(m);                            
                 }
                 childData.add(childDataItem);          
                 
                
                 arraylist.add(map);
              }
          } catch (JSONException e) {
              Log.e("Error", e.getMessage());
              e.printStackTrace();
          }
          return null;
      }

      @Override
      protected void onPostExecute(Void args) {
    	  
    	  progressBar.setVisibility(View.GONE);

      	// создаем адаптер
          ah = new MenuAdapterHelper(ctx, arraylist, childData);
          adapter = ah.getAdapter();
          
          elvMain = (ExpandableListView) v.findViewById(R.id.elvMain);
          elvMain.setAdapter(adapter);           
          
          elvMain.setOnChildClickListener(new OnChildClickListener() {
              public boolean onChildClick(ExpandableListView parent, View v,
                  int groupPosition,   int childPosition, long id) {
                Log.d("myloga", "onChildClick groupPosition = " + groupPosition + 
                        " childPosition = " + childPosition + 
                        " id = " + id);
                
             ArrayList<HashMap<String, String>> res = (ArrayList<HashMap<String, String>>) getChild((int) groupPosition);
                
                 
                
            HashMap item = (HashMap) res.get(childPosition);
            
            ((MainActivity)getActivity()).yourPublicMethod(item);
                
            return false;
            
              }
              
          });
          
      	
      }
  }
  
}