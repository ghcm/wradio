package com.example.menufragments;

import java.util.ArrayList;
import java.util.Map;
import android.content.Context;
import android.widget.SimpleExpandableListAdapter;

public class MenuAdapterHelper {
  
  final String ATTR_GROUP_NAME= "groupName";
  final String ATTR_PHONE_NAME= "phoneName";
  
  // коллекция для групп
  ArrayList<Map<String, String>> groupData;
  
  // коллекция для элементов одной группы
  ArrayList<Map<String, String>> childDataItem;

  // общая коллекция для коллекций элементов
  ArrayList<ArrayList<Map<String, String>>> childData;
  // в итоге получится childData = ArrayList<childDataItem>
  
  // список аттрибутов группы или элемента
  Map<String, String> m;
  
  Context ctx;
  
  MenuAdapterHelper(Context _ctx, ArrayList<Map<String, String>> arraylist, ArrayList<ArrayList<Map<String, String>>> _childData) {
    ctx = _ctx;
    groupData = arraylist;
    childData = _childData;
  }
  
  SimpleExpandableListAdapter adapter;
  
  
  SimpleExpandableListAdapter getAdapter() {
        // список аттрибутов групп для чтения
        String groupFrom[] = new String[] {ATTR_GROUP_NAME};
        // список ID view-элементов, в которые будет помещены аттрибуты групп
        int groupTo[] = new int[] {android.R.id.text1};
       
        // список аттрибутов элементов для чтения
        String childFrom[] = new String[] {"title"};
        // список ID view-элементов, в которые будет помещены аттрибуты элементов
        int childTo[] = new int[] {android.R.id.text1};
        
        adapter = new SimpleExpandableListAdapter(
            ctx,
            groupData,
            android.R.layout.simple_expandable_list_item_1,
            groupFrom,
            groupTo,
            childData,
            android.R.layout.simple_list_item_1,
            childFrom,
            childTo);
        
    return adapter;
  }
  
  String getGroupText(int groupPos) {
    return ((Map<String,String>)(adapter.getGroup(groupPos))).get(ATTR_GROUP_NAME);
  }
  
  String getChildText(int groupPos, int childPos) {
    return ((Map<String,String>)(adapter.getChild(groupPos, childPos))).get(ATTR_PHONE_NAME);
  }
  
  String getGroupChildText(int groupPos, int childPos) {
    return getGroupText(groupPos) + " " +  getChildText(groupPos, childPos);
  }
}