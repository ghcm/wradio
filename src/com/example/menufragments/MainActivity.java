package com.example.menufragments;

import java.util.HashMap;

/*import com.example.expandable.MyService;
import com.example.expandable.R;*/


/*import com.example.expandable.MainActivity;
import com.example.expandable.R;*/



//import com.example.expandable.MainActivity;

import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends Activity {
  
  MenuMainFragment menuMainFragment;
  MenuItemFragment menuItemFragment;
  NewsMainFragment newsMainFragment;
  NewsItemFragment newsItemFragment;
 
  FragmentTransaction fTrans;
  CheckBox chbStack;
  static HashMap menuItem;
  static HashMap newsItem;
  
  static String newsPicture = "image";
  static String newsTitle = "title";
  static String newsText = "text";
  static Context contextApps;
  
  Button btnMenu, btnNews;
  static ImageButton playPauseBtn;
  public static boolean isPlaying = false;
  MediaPlayer mediaPlayer;
  AudioManager am;
  static Boolean isNetAvailable;
  
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    
    menuMainFragment = new MenuMainFragment();
    menuItemFragment = new MenuItemFragment();
    
    newsMainFragment = new NewsMainFragment();
    newsItemFragment = new NewsItemFragment();
    
    contextApps =  getApplicationContext();
    
    if (isNetworkAvailable()) {
    	isNetAvailable = true;
    }
    else {
    	Toast.makeText(this, "Internet connection error", Toast.LENGTH_LONG).show();
    	isNetAvailable = false;
    }
    
    ActionBar mActionBar = getActionBar();        
    MainActivity.customizeActionBar(this, mActionBar);
    
  }

  public void onClick(View v) {
    fTrans = getFragmentManager().beginTransaction();
    switch (v.getId()) {
    case R.id.btnMenu:
      fTrans.replace(R.id.frgmCont, menuMainFragment);
      break;
    case R.id.btnNews:
      fTrans.replace(R.id.frgmCont, newsMainFragment);
      break;   
    default:
      break;
    }
    fTrans.addToBackStack(null);
    fTrans.commit();
  }
  
  static public HashMap getMenuItem(){
	  return menuItem;
  }
  
  static public HashMap getNewsItem(){
	  return newsItem;
  }
  
  public void setNewsItemFragment(HashMap item) {
	  fTrans = getFragmentManager().beginTransaction();
	  fTrans.replace(R.id.frgmCont, newsItemFragment);
	  fTrans.addToBackStack(null);
	  fTrans.commit();
	  newsItem = item;
  } 
  
  public void yourPublicMethod(HashMap item){
	  fTrans = getFragmentManager().beginTransaction();
	  fTrans.replace(R.id.frgmCont, menuItemFragment);
	  fTrans.addToBackStack(null);
	  fTrans.commit();	
	  menuItem = item;
  }
  
  
  static void customizeActionBar(Context ctx, ActionBar mActionBar) {
  	
	  
      mActionBar.setDisplayShowHomeEnabled(false);
      mActionBar.setDisplayShowTitleEnabled(false);
      LayoutInflater mInflater = LayoutInflater.from(ctx);

      View mCustomView = mInflater.inflate(R.layout.custom_action_bar, null);


      MainActivity.playPauseBtn = (ImageButton) mCustomView.findViewById(R.id.playPauseBtn);

      mActionBar.setCustomView(mCustomView);
      mActionBar.setDisplayShowCustomEnabled(true);
      
      if (isPlaying) {
    	  MainActivity.playPauseBtn.setBackgroundResource(R.drawable.pause);
      }
	 
}
  
  public void startStopRadio(View v) {  
  	if (!isNetAvailable) return;
  	
      if (!isPlaying) {
          startService(new Intent(this, MyService.class));
          playPauseBtn.setBackgroundResource(R.drawable.pause);
      } else {
          stopService(new Intent(this, MyService.class));
          playPauseBtn.setBackgroundResource(R.drawable.play);
      }

  }
  
  private boolean isNetworkAvailable() {
      ConnectivityManager connectivityManager 
            = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
      NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
      return activeNetworkInfo != null && activeNetworkInfo.isConnected();
  }
  
}