package com.example.menufragments;


import android.app.ListFragment;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class NewsMainFragment extends ListFragment {


    ProgressDialog mProgressDialog;
    ArrayList<HashMap<String, String>> arraylist;
    JSONObject jsonobject;
    JSONArray jsonarray;
    ListView listview;
    NewsAdapterHelper adapter;
    

    HashMap<String, String> resultp = new HashMap<String, String>();
    String data[] = new String[] { "one", "two", "three", "four" };
    String dataUrl = "http://easyscript.me/api/news/";
    
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        new DownloadJSON().execute();
    }


    private class DownloadJSON extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            // Create an array
            arraylist = new ArrayList<HashMap<String, String>>();
            // Retrieve JSON Objects from the given URL address
            jsonobject = JSONfunctions
                    .getJSONfromURL(dataUrl);



            try {
                // Locate the array name in JSON
                jsonarray = jsonobject.getJSONArray("data");

                for (int i = 0; i < jsonarray.length(); i++) {
                    HashMap<String, String> map = new HashMap<String, String>();
                    jsonobject = jsonarray.getJSONObject(i);
                    // Retrive JSON Objects
                    map.put(MainActivity.newsText, jsonobject.getString("text"));
                    map.put(MainActivity.newsTitle, jsonobject.getString("title"));
                    map.put(MainActivity.newsPicture, jsonobject.getString("image"));
                    // Set the JSON Objects into the array
                    arraylist.add(map);
                }
            } catch (JSONException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            adapter = new NewsAdapterHelper(MainActivity.contextApps, arraylist);
            setListAdapter(adapter);
        }
    }



    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
       // Toast.makeText(getActivity(), "position = " + position, Toast.LENGTH_SHORT).show();

        resultp = arraylist.get(position);
        
        ((MainActivity)getActivity()).setNewsItemFragment(resultp);

       /* Toast.makeText(getActivity(), "newsText = " + resultp.get(MyActivity.newsText), Toast.LENGTH_SHORT).show();
        Toast.makeText(getActivity(), "newsTitle = " + resultp.get(MyActivity.newsTitle), Toast.LENGTH_SHORT).show();
        Toast.makeText(getActivity(), "newsPicture = " + resultp.get(MyActivity.newsPicture), Toast.LENGTH_SHORT).show();*/
    }
}