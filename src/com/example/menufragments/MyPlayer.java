package com.example.menufragments;

import java.io.IOException;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;

import android.util.Log;

public class MyPlayer implements OnPreparedListener, OnCompletionListener {

    final String LOG_TAG = "myLogs";

    final String DATA_STREAM = "http://online.radiorecord.ru:8101/rr_128";

    //final String DATA_STREAM = "http://eu7.101.ru:8000/c18_8?tok=28382690&mode=h58sh1406029988";
    MediaPlayer mediaPlayer;

    void MyPlayer() {	}

    public void play() {


        try {
            Log.d(LOG_TAG, "start Stream");
            mediaPlayer = new MediaPlayer();
            Log.d(LOG_TAG, "1");
            mediaPlayer.setDataSource(DATA_STREAM);
            Log.d(LOG_TAG, "2");
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            Log.d(LOG_TAG, "3");
            Log.d(LOG_TAG, "prepareAsync");
            mediaPlayer.setOnPreparedListener(this);
            Log.d(LOG_TAG, "4");
            mediaPlayer.prepareAsync();
            Log.d(LOG_TAG, "5");
            MainActivity.isPlaying = true;
        } catch (IOException e) {
            Log.d(LOG_TAG, "io");
            e.printStackTrace();
        }
        if (mediaPlayer == null) {
            Log.d("myLogs", "NULL");
            return;
        }



        mediaPlayer.setOnCompletionListener(this);
    }

    private void releaseMP() {
        if (mediaPlayer != null) {
            try {
                mediaPlayer.release();
                mediaPlayer = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        Log.d(LOG_TAG, "onPrepared");
        mp.start();
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        Log.d(LOG_TAG, "onCompletion");
    }

    public void onDestroy() {
        //  super.onDestroy();
        MainActivity.isPlaying = false;
        releaseMP();
    }


}