package com.example.menufragments;

import java.util.ArrayList;
import java.util.HashMap;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;



public class NewsAdapterHelper  extends BaseAdapter {
    Context ctx;
    LayoutInflater lInflater;
    ArrayList<HashMap<String, String>> objects;
    protected ImageLoader imageLoader = ImageLoader.getInstance();

    HashMap<String, String> resultp = new HashMap<String, String>();

    NewsAdapterHelper(Context context, ArrayList<HashMap<String, String>> arraylist) {

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context).build();
        ImageLoader.getInstance().init(config);

        ctx = context;
        objects = arraylist;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    // кол-во элементов
    @Override
    public int getCount() {
        return objects.size();
    }

    // элемент по позиции
    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    // id по позиции
    @Override
    public long getItemId(int position) {
        return position;
    }

    // пункт списка
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // используем созданные, но не используемые view
        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.news_listview_item, parent, false);
        }

        final int pos = position;

        resultp = objects.get(position);

        ImageView picture = (ImageView) view.findViewById(R.id.newsImage);
        TextView newsTitle = (TextView) view.findViewById(R.id.newstitle);

        newsTitle.setText(resultp.get(MainActivity.newsTitle));

        imageLoader.displayImage(resultp.get(MainActivity.newsPicture), picture);
      
        return view;
    }
}
