package com.example.menufragments;

import java.util.HashMap;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class NewsItemFragment extends Fragment {
	
	

	TextView titleText;
	TextView priceText;
	TextView desText;
	ImageView imageElement;

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
	  
	  View v = inflater.inflate(R.layout.singlenews, container, false);
      
    return v;
  }
  
  @Override
  public void onActivityCreated (Bundle savedInstanceState) {
      super.onActivityCreated(savedInstanceState);
      
      HashMap item = MainActivity.getNewsItem();

      
      
      String newsTitle = (String) item.get(MainActivity.newsTitle);
      String  newsText = (String) item.get(MainActivity.newsText);
		
		TextView titleText = (TextView) getView().findViewById(R.id.newsheader);
		TextView text = (TextView) getView().findViewById(R.id.newsText);
		

		titleText.setText(newsTitle);
		text.setText(newsText);	
  }
  

}